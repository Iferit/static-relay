/* Project: Prometheus
 * Date: 10/19/2017
 * Programmer: Aaron Effinger
 * Rev:1
 * 
 * Description:
 * The Device methos handels the power system, aswell as connecting deviceses to other devices. This class determines the base structure for all devices. It defines the following:
 *  - How devices are connected.
 *  - Which devices can power them and how.
 *  - What happens when a device is powered.
 *  - What happens when a device is turned off.
 * 
 * Log:
 * Date:			Rev		Description
 * 
 */

using System.Collections.Generic;
using UnityEngine;

public enum DeviceState {PoweringUp,Powered,PoweringDown,Unpowered,Disabled};

public class Device : MonoBehaviour
{
	[Header("State")]
	public DeviceState state = DeviceState.Unpowered;
	public bool activated = false;
	[Header("Proporties")]
	public bool multipalActivations = false;
	public float powerConsumption=0f;
	int numPowerSources = 0;
	[Header("Sound")]
	public AudioClip powerUpSound;
	[Range(0, 1)]
	public float powerUpIntensity = 1f;
	public AudioClip powerDownSound;
	[Range(0, 1)]
	public float powerDownIntensity = 1f;
	public AudioClip activationSound;
	[Range(0, 1)]
	public float activateIntensity = 1f;
	public AudioClip disabledSound;
	[Range(0, 1)]
	public float disableIntensity = 1f;
	AudioSource audioPlayer;
	public float volLow = .5f;
	public float volHigh = 1f;
	[Header("Other Devices")]
	public List<Device> connectedDevices;
	public GameObject[] subComponents;

	private void Awake()
	{
		audioPlayer = GetComponent<AudioSource>();
	}

	public void PowerUp()
	{
		numPowerSources++;
		if ((state == DeviceState.Unpowered || state==DeviceState.PoweringDown) && numPowerSources > 0)
		{
			state = DeviceState.PoweringUp;
			OnPowerUp();
		}
	}

	public void PowerDown()
	{
		numPowerSources--;
		if ((state == DeviceState.Powered || state == DeviceState.PoweringUp) && numPowerSources < 1)
		{
			state = DeviceState.PoweringDown;
			OnPowerDown();
		}
	}

	public virtual void OnPowerUp()
	{
		audioPlayer.PlayOneShot(powerUpSound, Random.Range(volLow,volHigh)*powerUpIntensity);
		state = DeviceState.Powered;
		foreach(GameObject obj in subComponents)
		{
			obj.SetActive(true);
		}
	}

	public virtual void OnPowerDown()
	{
		audioPlayer.PlayOneShot(powerDownSound, Random.Range(volLow, volHigh)*powerDownIntensity);
		state = DeviceState.Unpowered;
		foreach (GameObject obj in subComponents)
		{
			obj.SetActive(false);
		}
	}

	public virtual void Activate()
	{
		if(state == DeviceState.Powered && (activated == false || multipalActivations == true))
		{
			//preform any activation animations, sounds, or methods first
			activated = true;

		}
	}



	public virtual void StartPowered()
	{
		numPowerSources++;
		state = DeviceState.Powered;
		foreach (GameObject obj in subComponents)
		{
			obj.SetActive(true);
		}
	}

	private void OnDrawGizmos()
	{
		if (connectedDevices != null)
		{
			foreach(Device dev in connectedDevices)
			{
				GameObject tar = dev.gameObject;
				Vector3 midPoint = new Vector3();
				// midPoint = requiredSwitch.transform.position - (transform.position/2);
				midPoint = (transform.position / 2) + (tar.transform.position / 2);
				Gizmos.color = Color.green;
				Gizmos.DrawLine(transform.position, midPoint);
				Gizmos.color = Color.red;
				Gizmos.DrawLine(midPoint, tar.transform.position);
			}
		}
	}
}
