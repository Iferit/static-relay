﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerSource : MonoBehaviour {
	public List<Device> connectedDevices;
	public List<Device> powering;

	private void Awake()
	{
		foreach (Device dev in connectedDevices)
		{
			if (!powering.Contains(dev))
			{
				dev.StartPowered();
				powering.Add(dev);
			}
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		Device device = other.gameObject.GetComponent<Device>();

		if (device!=null && !powering.Contains(device))
		{
			powering.Add(device);
			device.PowerUp();
		}
	}

	private void OnTriggerExit(Collider other)
	{
		Device device = other.gameObject.GetComponent<Device>();

		if (device != null && powering.Contains(device) && !connectedDevices.Contains(device))
		{
			powering.Remove(device);
			device.PowerDown();
		}
	}

	private void Update()
	{
		foreach(Device dev in connectedDevices)
		{
			if (!powering.Contains(dev))
			{
				dev.PowerUp();
				powering.Add(dev);
			}
		}
	}

	private void OnDrawGizmos()
	{
		if (connectedDevices != null)
		{
			foreach (Device dev in connectedDevices)
			{
				GameObject tar = dev.gameObject;
				Vector3 midPoint = new Vector3();
				// midPoint = requiredSwitch.transform.position - (transform.position/2);
				midPoint = (transform.position / 2) + (tar.transform.position / 2);
				Gizmos.color = Color.green;
				Gizmos.DrawLine(transform.position, midPoint);
				Gizmos.color = Color.blue;
				Gizmos.DrawLine(midPoint, tar.transform.position);
			}
		}
	}

}
