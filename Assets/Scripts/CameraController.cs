﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour {
	public CinemachineVirtualCamera vCam;
	public static List<CinemachineVirtualCamera> vCams;

	private void Awake()
	{
		vCams = new List<CinemachineVirtualCamera>();
		GameObject[] vCamsObjects = GameObject.FindGameObjectsWithTag("VCam");
		foreach(GameObject go in vCamsObjects)
		{
			CinemachineVirtualCamera goCam = go.GetComponent<CinemachineVirtualCamera>();
			if (goCam!=null && !vCams.Contains(goCam))
			{
				vCams.Add(goCam);
			}
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			foreach(CinemachineVirtualCamera cam in vCams)
			{
				if (cam == vCam)
				{
					vCam.Priority = 50;
				}
				else
				{
					cam.Priority = 1;
				}
			}
		}
	}




}
