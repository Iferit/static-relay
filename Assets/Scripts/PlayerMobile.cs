using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMobile : MonoBehaviour {
	NavMeshAgent agent;
	public LayerMask hitMask;

	private void Start()
	{
		agent = GetComponent<NavMeshAgent>();
	}

	private void Update()
	{
		if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
		{
			// The screen has been touched so store the touch
			//Touch touch = new Touch();

			//if (Input.touchCount > 0)
			//{
			//	touch = Input.GetTouch(0);
			//}

			
				// If the finger is on the screen, move the object smoothly to the touch position
				//Vector3 touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10));
				RaycastHit hit;


			
				if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition),out hit,300, hitMask))
				{
					agent.destination = hit.point;
				}
		}
	}
}
