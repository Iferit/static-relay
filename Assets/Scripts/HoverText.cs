using UnityEngine;

public class HoverText : MonoBehaviour {
	#region Variables
	public Transform target;
	public Vector3 offset;
	public bool clampToScreen = false;
	public float clampBorderSize = 0.05f;
	public bool useMainCamera = true;
	public Camera cameraToUse;

	private Camera cam;
	private Transform thisTransform;
	private Transform camTransform;
	#endregion
	
	#region Unity Methods
	void Start () {
		thisTransform = transform;
		if (useMainCamera)
		{
			cam = Camera.main;
		}
		else
		{
			cam = cameraToUse;
		}
		camTransform = cam.transform;
	}
	
	void LateUpdate () {
		if (clampToScreen)
		{
			Vector3 relativePosition = camTransform.InverseTransformPoint(target.position);
			relativePosition.z = Mathf.Max(relativePosition.z, 1.0f);
			thisTransform.position = cam.WorldToViewportPoint(camTransform.TransformPoint(relativePosition + offset));
			thisTransform.position = new Vector3(Mathf.Clamp(thisTransform.position.x, clampBorderSize, 1.0f - clampBorderSize),
										 Mathf.Clamp(thisTransform.position.y, clampBorderSize, 1.0f - clampBorderSize),
										 thisTransform.position.z);
		}
		else
		{
			thisTransform.position = cam.WorldToViewportPoint(target.position + offset);
		}
	}
	
	#endregion
}
